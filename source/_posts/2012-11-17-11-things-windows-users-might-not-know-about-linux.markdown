---
layout: post
title: "11 Things Windows users might not know about Linux"
date: 2012-11-17 23:43
comments: true
categories:
 - Linux
 - Operating Systems
 - Top 10
---

As an avid Linux user for over 5 years, it still surprises me how many people don't know basic things about Linux which make it so appealing to people like me. If you're thinking about using Linux for the first time, or perhaps you've only just heard about it, here are a few things you might now know that might interest you:

<!-- More -->

##1: Linux isn't an operating system

Linux is actually the name given the "kernel" of one type of operating system. There are hundreds of thousands of Linux distributions for you to choose from. Linux draws its inheritance back from UNIX which was a proprietary operating system back in the 70s. We call the entire thing "Linux" because its easier and simpler for people who are new to Linux. Each one of those versions of Linux is referred to as a "distribution" or "flavour". Ubuntu is a popular distribution of Linux, as is RedHat, Fedora, SuSE and Debian. Some people like to call linux GNU/Linux, because the operating system runs a suite of tools called the GNU toolkit on top of its self, in order for it to be complete.

##2: You don't have to install Linux to try it

You can try Linux by running it inside a virtual machine, or by running a LiveCD or LiveUSB version, which allows you to run the entire operating system from a drive instead of on your harddrive! This means you can try it out (at a reduced speed) to check to see how well things work for you, and for your computer.

##3: You can have multiple desktops

One of the cool things Windows users often can't even fathom until they use it or see someone using it, is the concept of _multiple_ desktops. Imagine being able to have many of the good aspects of "multiple monitors", but with only one desktop. Using keybindings (often ctrl+alt+left or ctrl+alt+right), you can switch between them instantly to give you a nice clean desktop to focus on what you're doing. This is such a handy feature that Apple stole it for OSX (they call it "spaces").


##4: Often, Linux requires less configuration than Windows

In Windows, if you have to install new hardware, you will almost always have to use an installation CD to get it working, or (good luck!) trawl the internet to find the right driver. Often, people resort to paying for programs to do this for you.

Often, your Linux distribution will pick up and install new hardware for you automatically. If the driver is Open Source and supported by your distribution, it will just work! A breath of fresh air to many people out there.

##5: You can customise it heavily if you want to

If it tickles your fancy, you can change almost every part of any Linux operating system. If you're skilled enough, you can remove or swap large swathes of it to suit your tastes or needs. For example, its possible (and easy) to change the entire graphical environment to something else. That's right - when you use Linux, that doesn't necessarily mean you have to use the _same_ Linux as everyone else. For this reason, it's a customiser's dream.

##6: It runs great on older machines

Linux is very good at being small and light if you want it to be. Linux is known to be used in microwaves, refridgerators, cars, DVD players, in-flight entertainment systems, and other small, low-powered devices. So if you have an old machine you want to donate, for example, putting Linux on it might be a great way of giving the machine a new lease of life.

##7: It has a command system that lets you build little programs on the fly to do mundane tasks for you

One of the most powerful things about Linux is the terminal (known as the command-line on Windows, but it's a little bit different). Now, many Windows users are scared by the terminal, and that's OK - you don't need to use it at all to use Linux. But for power users, it makes for a very fast, convenient system to do mundane tasks quickly. One of the most powerful things you can do is called "piping". Piping is where you take the output of one command and you "pipe" it into another command as an input. This lets you chain operations into one big command that happens straight away! If you find your little command particularly useful, you can paste it into a file, call it whatever.__sh__ and then you have a little program you can run again and again.

##8: It's free, but backed by huge companies.

I think these days, most Windows users that have heard _of_ Linux know that it's free. But just in case you haven't heard, yes - it is free.

However that's not necessarily what a company might look for in an Operating System. The good news is, distributions like Ubuntu are supported by a company called Canonical, which makes most of its money in supporting businesses. So you end up with a free product, and a paid technical support line, which is a heck of a lot cheaper than installing Windows on lots of machines, and _then_ having to pay support.

##9: You don't need to remember to check for updates for all of your applications - it does it for you

This is dependant on the distribution you use: all allow you to update _all_ your applications in one fell swoop, but some even download the updates for you, so all you have to do is to decide when you want to install them.

##10: You don't need to restart your computer a gazzilion times to get all the updates

Most Linux distributions will update be able to do system updates in one operation, which means its much faster and more convenient for you.

##11: It can run Windows applications

Not many newbies know this, but there's a big open-source project known as WINE (which is a recursive alliteration for "WINE is not an emulator") which aims to allow Linux users to execute windows binaries and have them converted into something Linux can understand.

WINE is by no means perfect, but often you'll find that if there isn't an alternative for that obscure application you need to run, you can probably run it on WINE. Even many games can be run on WINE, and some - such as the ever-popular _world of warcraft_, often run faster than when run on Windows, ironically. Definitely something to try out and another reason most people can switch without much fuss.
