---
layout: post
title: "What makes for a Good Development Lead?"
author: "John Hamelink"
date: 2012-11-11 21:31
comments: true
categories:
 - Development
 - Self Improvement
---

Throughout my career so far, I've mostly worked alone. When I say alone, I've
often worked alongside other people, but ultimately I've been paying them, or I
had made the choice about whether they were suitable for the project (or for me)
or not. They've mostly not been working on the same thing I've been working on
either.

<!-- More -->

Ultimately, I've been the leader of the projects I've worked on during most of
my freelance career. While at my current fulltime job however, due to the nature
of the work I was doing there, I often had to work with other people who I
couldn't choose. This was a great new experience for me, with some terrible 3rd
parties from other companies I had to integrate with, and some truly amazing
developers I got the chance to work alongside. I learned a lot about what I look
for in a leader during my time there, based on excellent leadership skills I saw
working with some of the people I worked with, and with other people I saw
(mostly in other companies I'm happy to report) make mistakes, I learned how I
myself could strive to be that golden leader I wanted so bad. While I don't
claim to be perfect, I feel I have manage to construct a reasonably good
guideline on how I should try to conduct myself in order to be the leader I
would want to lead me.

##Passion

Yes, I know, the whole "you must be passionate" thing is a bit dry/old these
days, but it's true. There is nothing more uninspiring than working with or for
someone who doesn't care about the project you're building. It is thoroughly
demotivational. Fitting the right character with the right project is really
important if you want to inspire your team to be passionate themselves about the
project. An uninterested leader will create an uninterested team.

An uninterested team will generate (not build/create/produce) a lacklustre
product at a high cost. A highly interested team will be motivated to put in
that extra little slice of effort which can take a project from being mediocre
to being utterly breathtaking.

##Compassion

A good leader should be interested in his team, on an individual level. The
difference between an average and truly brilliant leader is how well they can
relate to their team. Build a team based on how well you gel just as much as
their technical aptitude and prior experience. A truly interested leader won't
talk about work the whole time - they'll try and build a relationship with each
member of the team, in order to encourage each team member to build an emotional
attachment to their work and their fellow teammates and leader.

Talking about life out of work is also important - breaking those traditional
"I'm a boss" barriers really helps the team bond together as a whole, and help
them feel free to speak up about things they think are important to the project
you're all working on together.

Remember: every person in your team has hopes and dreams. Embrace that, as
it's actually a trump card - you, as team lead, can help bring a positive aspect
to that person's life, which can help them - directly or indirectly - reach
those hopes and dreams. Don't make it taboo to talk about competitors or change.
A caged bird is more likely to dream of freedom.

Also, remember that there's a fine line between caring and creepy, walk that
line carefully because you don't want to disillusion your team.

##A Culture of Change and of Pace

Stagnancy is the rot of any team. Don't allow your team to become idle or
stagnant. Promote a culture of constant self-improvement and perfectionism which
will help fill in gaps in work if your team is waiting for another team or
company to finish their bit before you can continue yours.

Embrace change as part of your company's culture, as it is
realistically a business reality that the business and company should (and will)
change. Making that part of the business makes those really hard changes easier.

Keep the core goals of the project set in stone, but allow your team to be as
creative as they want in order to reach those goals. Only if they look on track
to miss the goals should you begin to pull in those reigns.

##Don't Try to be a "Boss", Try to be a Friend

Show your team that you're human, a good comrade, someone who people can let
their hair down around you. You will get the best results from them if they feel
that way, without a doubt.
