---
layout: post
title: "The Green Developer"
date: 2012-11-12 18:16
comments: true
categories:
 - Philosophy
 - Development
 - Self Improvement
---

A good developer is energy conscious. A good developer understands the impact he
has on the environment around him. A good developer is able to take all these
things and use them to make a positive impact in his universe. A good developer
is a green developer.

<!-- More -->

##A green developer? What are you smoking?

A green developer understands how to build in blocks: he must master the art of
software _engineering_ versus software developing. What sets the software
engineer apart from the developer is not speed, agility or knowledge, it is the
ability to recognise and compartmentalise everything he does into reusable
chunks. Why? Simply because reusable code means better code and also half the
code.

##Sounds Slow and Painful...

![The reaction of some developers when I say that good developers don't copy and paste](http://dc544.4shared.com/img/_DQO5gfz/s7/AINT_NOBODY_GOT_TIME_FOR_THAT.gif "The reaction of some developers when I say that good developers don't copy and paste")

It really isn't that painful. But guess what is? Rehashing your code because
business requirements change.

The trick is using [design patterns](http://en.wikipedia.org/wiki/Design_patterns).
These structures allow us to engineer abstract concepts which can then be used
in tandem to produce the desired effect, while still remaining flexible enough
for us to use these things elsewhere.

A good example of a design pattern which allows for code reuse, cleanliness, and
speeds up development (within the context of web development) is an [MVC
framework](http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller).
The MVC ("Model, View, Controller") pattern allows for great amount of
flexibility while speeding up development by allowing us to control a flow while
separating different parts of the execution (defining what it is we want to do,
obtaining data from sources, and displaying it within the context of a
file-type).

But using a framework and then slapping the same old sloppy code within it
doesn't give you much of an advantage. The next step is using these patterns
within libraries to allow you to separate out parts of your system into their
own chunks that can be imported into other projects.

##Green Developers don't use band-aids

![What bugfixes should not look like](http://i.imgur.com/eZQf1.jpg "What bugfixes should not look like")

Band-aid bugfixes don't solve problems. We've all been there: deadlines looming,
clients breathing down proverbial necks, but we must all remember that a crappy
bug fix is the same as pushing 3 new bugs to the bottom of your bug-list. Fix the
problem _well_ and you'll only need to fix it _once_.


##Summary

A green developer:

 - Recycles his code
 - Makes use of design patterns to enable him to do that
 - Looks to the future when he makes his bug fixes
 - Understands that he's investing time for a sustainable future

Clichéd metaphor aside, any developer who follows these simple rules will be
well on his way to saving himself a heck-of-a-lot of time and effort which will
allow him to concentrate more on the interesting bits of the job: solving all
the hard problems.
